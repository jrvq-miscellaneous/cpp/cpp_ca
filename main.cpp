/*
* Author: Jaime Rivera
* Date : 2020.03.22
* Revision : 2020.04.11
* Copyright : Copyright 2020 Jaime Rivera
* Brief: Basic implementation of cellular automata
* Credits: Sean Barrett, author of the STB library, used in this project (https://github.com/nothings/stb)
*/


#include <iostream>
#include <random>
#include <string>
#include <direct.h>

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <stb_image_write.h>


// --------- CONSTANTS --------- //

// OUTPUT DIR
std::string OUTPUT_FOLDER = "output_frames";

// OUTPUT PARAMETERS
const int FRAMES_TO_WRITE = 300;
const int WIDTH = 192;
const int HEIGHT = 108;
const short CHANNELS = 4;

// RULE SETS
int all_rules[] = { 30, 90, 73 };   // TODO: These are just some examples, substitute for an input question


// --------- CELLS FRAME --------- //
bool old_state[WIDTH][HEIGHT];
bool new_state[WIDTH][HEIGHT];
int calculate_rule_output(int x, int y, int rule_number);


// --------- MAIN --------- //
int main()
{
	for (int rls : all_rules)
	{
		// RULE NAME
		std::string rule_name = "rule_" + std::to_string(rls);

		// ARRAY POPULATION
		for (int y = 0; y < HEIGHT; y++)
		{
			for (int x = 0; x < WIDTH; x++)
			{
				new_state[x][y] = false;

				if (y == HEIGHT - 1 && x == WIDTH / 2)
				{
					new_state[x][y] = true;
				}
			}
		}

		// FRAME WRITING
		std::string out_dir = OUTPUT_FOLDER + "_" + rule_name;
		const char *out_folder = out_dir.c_str();
		int mkdir_result = _mkdir(out_folder);
		for (int frame = 0; frame < FRAMES_TO_WRITE; frame++)
		{
			// Storing the old values
			for (int y = 0; y < HEIGHT; y++)
			{
				for (int x = 0; x < WIDTH; x++)
				{
					old_state[x][y] = new_state[x][y];
				}
			}

			// Calculating new values
			for (int y = 0; y < HEIGHT; y++)
			{
				for (int x = 0; x < WIDTH; x++)
				{
					if (y != HEIGHT - 1)
					{
						new_state[x][y] = old_state[x][y + 1];
					}
					else
					{
						bool rl_output = calculate_rule_output(x, y, rls);
						new_state[x][y] = rl_output;
					}

				}
			}

			// Pixel colors
			unsigned char *pixels = new unsigned char[WIDTH * HEIGHT * CHANNELS];

			int index = 0;
			for (int j = 0; j < HEIGHT; j++)
			{
				for (int i = 0; i < WIDTH; i++)
				{
					bool state = old_state[i][j];

					int color;
					color = state == true ? 255 : 0;
					pixels[index++] = color; // R
					pixels[index++] = color; // G
					pixels[index++] = color; // B
					pixels[index++] = color; // Alpha
				}
			}


			// Writing the output file
			if (frame < 0) { continue; }
			const std::string out_filename_string = out_dir + "/ca_" + rule_name + "_" + std::to_string(frame) + ".png";
			const char *out_filename = out_filename_string.c_str();
			stbi_write_png(out_filename, WIDTH, HEIGHT, CHANNELS, pixels, WIDTH * CHANNELS);
			delete[] pixels;

		}
	}

	return 0;
}


// --------- AUX FUNCTIONS --------- //
int calculate_rule_output(int x, int y, int rule_number)
{
	// Index for the rule number
	bool left, center, right;
	if (x == 0)
	{
		left = old_state[WIDTH - 1][y];
		center = old_state[x][y];
		right = old_state[x + 1][y];
	}
	else if (x + 1 == WIDTH)
	{
		left = old_state[x -1][y];
		center = old_state[x][y];
		right = old_state[0][y];
	}
	else
	{
		left = old_state[x - 1][y];
		center = old_state[x][y];
		right = old_state[x + 1][y];
	}

	int bit_pos = 0;
	bit_pos = right  << 0 | bit_pos;
	bit_pos = center << 1 | bit_pos;
	bit_pos = left   << 2 | bit_pos;

	// Bit state
	bool bit_status = (rule_number >> bit_pos) & 1;
	return bit_status;
}